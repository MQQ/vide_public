#+
#   VIDE -- Void IDentification and Examination -- ./crossCompare/plotting/datasetsToPlot.py
#   Copyright (C) 2010-2014 Guilhem Lavaux
#   Copyright (C) 2011-2014 P. M. Sutter
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; version 2 of the License.
# 
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#+
#!/usr/bin/env python


workDir = "/home/psutter2/workspace/Voids/"
figDir  = "./figs"

sampleDirList = [ 
#                  "multidark/md_ss0.1_pv/sample_md_ss0.1_pv_z0.56_d00/",
#                  "multidark/md_halos_min1.393e12_pv/sample_md_halos_min1.393e12_pv_z0.56_d00/",
#                  "random/ran_ss0.000175/sample_ran_ss0.000175_z0.56_d00/",
#                  "random/ran_ss0.1/sample_ran_ss0.1_z0.56_d00/",
#                  "multidark/md_hod_dr9mid_pv/sample_md_hod_dr9mid_pv_z0.56_d00/",
#                  "multidark/md_ss0.000175_pv/sample_md_ss0.000175_pv_z0.56_d00/",
                  "sdss_dr9/sample_lss.dr9cmassmid.dat/",
                  "lanl/masked/masked_lanl_hod_dr9mid_pv/sample_masked_lanl_hod_dr9mid_pv_z0.5/" ]

dataPortion = "central"

